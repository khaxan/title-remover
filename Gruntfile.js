module.exports = function (grunt) {

	grunt.initConfig({
		makepot: {
			target: {
				options: {
					domainPath: 'languages/',
					type: 'wp-plugin',
				}
			}
		},
	});

	grunt.loadNpmTasks('grunt-wp-i18n');
	grunt.registerTask('default', ['makepot']);
};
